import React from "react";
import "./Header.css";

const Header = (props) => {
  return (
    <header>
      <div className="header">
        <div className="header-option">
          <h1>{props.title}</h1>
          
        </div>
      </div>
    </header>
  );
};

export default Header;
