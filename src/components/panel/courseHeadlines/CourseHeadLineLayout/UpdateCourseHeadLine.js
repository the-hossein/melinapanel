import { Button, TextareaAutosize, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
import {
  BaseUrl,
  GET_ONE_COURSEHEADLINE,
  UPDATE_COURSEHEADLINE,
  UPLOAD_VIDEO,
} from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import UploadAsset from "../../../../tools/UploadImage/UploadAsset/UploadAsset";
import Header from "../../../Header/Header";

const UpdateCourseHeadLine = () => {
  const user = useSelector((state) => state.User.Data);
  const [preload, setPreload] = useState(true);
  const [courseId, setCourseId] = useState(0);
  const [courseheadlineId, setCourseheadlineId] = useState(0);



  const [video, setVideo] = useState("");
  const [videoId, setVideoId] = useState("");

  const [titleCourse, setTitleCourse] = useState("");
  const [titleEnCourse, setTitleEnCourse] = useState("");


  const [durationCourse, setDurationCourse] = useState("");

  const [descriptionCourse, setDescriptionCourse] = useState("");
  const [descriptionEnCourse, setDescriptionEnCourse] = useState("");

 

  const setVideoCourse = async (e) => {
    setPreload(true);
    var myHeaders = new Headers();
    // myHeaders.append("Content-Type", "application/json");

    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);

    try {
      const result = await CallApi(
        BaseUrl + UPLOAD_VIDEO,
        formdata,
        myHeaders,
        "POST"
      );
      console.log(result);
      if (result[1] === 200) {
        let id = result[0].data.id;
        setVideoId(id);
        /*     element.src=result[0].data.filePath; */
        let src = result[0].data.filePath;
        setVideo(src);
        setPreload(false);
        toast.success("ویدیو با موفقیت ثبت شد", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
       
        toast.error("خطا در آپلود ویدیو", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
    
      toast.error("خطا در آپلود ویدیو", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  const changeState = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case "title":
        setTitleCourse(value);
        break;
      case "titleEn":
        setTitleEnCourse(value);
        break;
    
      case "description":
        setDescriptionCourse(value);
        break;
      case "descriptionEn":
        setDescriptionEnCourse(value);
        break;
      case "duration":
        setDurationCourse(value);
        break;
     
      default:
        break;
    }
  };

  const AddCourseAction = async () => {
    setPreload(true);
    let raw = JSON.stringify({
      "id": courseheadlineId,
      "title": titleCourse,
      "titleen": titleEnCourse,
      "description":descriptionCourse,
      "descriptionen": descriptionEnCourse,
      "fileid": videoId,
      "courseid": courseId,
      "duration": durationCourse
    });

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
  

    try {
      const result = await CallApi(
        BaseUrl + UPDATE_COURSEHEADLINE,
        raw,
        myHeaders,
        "POST"
      );
      console.log(result);
      if (result[1]) {
        toast.success("بروزرسانی با موفقیت انجام شد", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        window.location.reload();
      } else {
        setPreload(false);
        toast.error("خطای سیستمی", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setPreload(false);
      toast.error("خطای سیستمی", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  useEffect(() => {

    const search = window.location.search; // could be '?foo=bar'
    const params = new URLSearchParams(search);
    // bar
    var id = params.get("id");

   
    setCourseheadlineId(id)
    console.log(id);
    getCourse(id);
  }, []);

  const getCourse = async (id) => {
    try {
      const response = await CallApi(
        BaseUrl + GET_ONE_COURSEHEADLINE + `${id}`,
        "{}",
        {},
        "GET"
      );
      if (response[1] === 200) {
        const { data } = response[0];
        /*    setPoster(data.fileImage.filePath);
           setPosterId(data.fileImage.id); */
        setVideo(data.video.filePath);
        setVideoId(data.video.id);
        setTitleEnCourse(data.titleEn);
        setTitleCourse(data.title);
     
        setDurationCourse(data.duration);
        /*   setLable(data.lable); */
        setDescriptionCourse(data.description);
        setDescriptionEnCourse(data.descriptionEn);
        setCourseId(data.courseId);
        setPreload(false);
      } else {
        toast.error("خطای سیستمی");
      }
    } catch {
      toast.error("خطای سیستمی");
    }
  };

  return (
    <>
      <Header title="بروز رسانی سرفصل " />
      {preload ? (
        <Loading />
      ) : (
        <Box
          table={true}
          child={
            <>
              {/*  <div className="flex-center center-row rtl">
                <UploadAsset
                  src={poster}
                  title="عکس دوره"
                  accept="image/*"
                  id="PrimaryImage"
                  Change={setImagePoster}
                />
              </div> */}
              <div className="flex-center center-row rtl">
                <UploadAsset
                  src={video}
                  title="ویدیو این جلسه"
                  accept="video/*"
                  id="previewvideoid"
                  video={true}
                  Change={setVideoCourse}
                />
              </div>
              <div className="row-fields">
                <TextField
                  value={titleCourse}
                  required
                  label="عنوان سرفصل"
                  name="title"
                  onChange={changeState}
                />
                <TextField
                  value={titleEnCourse}
                  required
                  label="عنوان انگلیسی سرفصل "
                  name="titleEn"
                  onChange={changeState}
                />
                {/*   <TextField
                  value={priceCourse}
                  required
                  label="قیمت دوره "
                  name="price"
                  onChange={changeState}
                /> */}
                <TextField
                  value={durationCourse}
                  required
                  label="مدت زمان دوره "
                  name="duration"
                  onChange={changeState}
                />
                {/* <TextField
                  value={lable}
                  required
                  label="برچسب دوره "
                  name="lable"
                  onChange={changeState}
                /> */}
                {/* <TextField
                value={ProductLable}
                onChange={(e) => ChangeLable(e)}
                label="عنوان دسته بندی"
              />
              <TextField
                required
                value={Weight}
                label="وزن"
                type="text"
                className="ltr-input"
                onChange={(e) => ChangeWeight(e)}
              />

              <TextField
                required
                value={Material}
                label="محتویات"
                type="text"
                className="ltr-input"
                onChange={(e) => ChangeMaterial(e)}
              /> */}
              </div>

              <div className="row-fields center-row">
                <TextareaAutosize
                  value={descriptionCourse}
                  labelId="Description"
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="توضیحات"
                  style={{ width: 480 }}
                  name="description"
                  onChange={changeState}
                />
                <TextareaAutosize
                  value={descriptionEnCourse}
                  labelId="DescriptionEn"
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="توضیحات انگلیسی"
                  style={{ width: 480 }}
                  name="descriptionEn"
                  onChange={changeState}
                />
              </div>
              <div className="row-fields"></div>
              <div className="row-fields">
                <Button onClick={AddCourseAction} variant="contained">
                  بروز رسانی سرفصل{" "}
                </Button>
              </div>
              <ToastContainer />
            </>
          }
        />
      )}
    </>
  );
};

export default UpdateCourseHeadLine;
