import React from "react";
import Container from "../../../LayoutContainers/Container";
import Nav from "../../../Navbar/Nav";
import AddCourseHeadLine from "./AddCourseHeadLine";
import UpdateCourseHeadLine from "./UpdateCourseHeadLine";

const CourseHeadLineLayout = ({ action }) => {
  return (
    <>
      <Nav />
      <Container child={action === "add" ? <AddCourseHeadLine /> : <UpdateCourseHeadLine />} />
    </>
  );
};

export default CourseHeadLineLayout;
