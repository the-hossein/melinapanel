import React, { useEffect, useState } from "react";
import { BaseUrl, GET_COURSE } from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import TableHeader from "../../../../tools/tableHeader/TableHeader";
import { PersionCurrency, PersionDate } from "../../../../tools/Tools";
import Pagination from "../../../../tools/Pagination/Pagination";
import Header from "../../../Header/Header";
import { TextField } from "@mui/material";
import { Link } from "react-router-dom";
import AddCircleOutlineIcon from "@material-ui/icons//AddCircleOutline";
import { IconButton } from "@material-ui/core";

const CoursesContainer = () => {
  const [preload, setPreload] = useState(true);
  const [AllCourse, setAllCourse] = useState("");
  const [showCourse, setShowCourse] = useState("");

  const getAllCourse = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    const course = await CallApi(BaseUrl + GET_COURSE, "{}", myHeaders, "get");
    if (course[1] === 200) {
      return course[0].data;
    }
  };

  useEffect(() => {
    const getData = async () => {
      let courseList = [];
      const data = await getAllCourse();
      data.reverse();
      for (var i = 0; i < data.length; i++) {
        var obj = {};
        obj["id"] = data[i].id;
        obj["title"] = data[i].title;
        obj["price"] = (await PersionCurrency(data[i].price)) + "تومان";
        let Pd = await PersionDate(data[i].createdDatetime);
        obj["date"] = Pd;
        courseList.push(obj);
      }
      setPreload(false);
      setAllCourse(courseList);
      setShowCourse(courseList)
    };
    getData();
  }, []);

  const SearchByName = (e) => {
    const targetSearch = AllCourse.filter(item => item.title.includes(e.target.value) );
    setShowCourse(targetSearch)
  }

  return (
    <>
      <Header title="مدیریت دوره ها" />
      {preload ? (
        <Loading />
      ) : (
        <Box
          table={true}
          child={
            <>
              <TextField
                id="outlined-basic"
                onChange={(e) => SearchByName(e)}
                variant="outlined"
                fullWidth
                label="Search"
              />
              <Link to="/panel/courses/add">
                <IconButton aria-label="primary" color="primary" size="large">
                  <AddCircleOutlineIcon fontSize="inherit" />
                </IconButton>
              </Link>

              <TableHeader items={["کد محصول", "نام", "قیمت", "تاریخ"]} />
              <Pagination
                perpage={10}
                datas={showCourse}
                link="/panel/courses/edit?id="
              />
            </>
          }
        />
      )}
    </>
  );
};

export default CoursesContainer;
