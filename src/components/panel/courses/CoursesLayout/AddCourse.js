import React, { useState } from "react";
import Box from "../../../../tools/box/Box";
import UploadAsset from "../../../../tools/UploadImage/UploadAsset/UploadAsset";
import Header from "../../../Header/Header";
import { Button, TextareaAutosize, TextField } from "@material-ui/core";
import { toast, ToastContainer } from "react-toastify";
import { useSelector } from "react-redux";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import {
  ADD_COURSE,
  BaseUrl,
  UPLOAD_IMAGE,
  UPLOAD_VIDEO,
} from "../../../../api/Url";
import Loading from "../../../../tools/Loading/Loading";

const AddCourse = () => {
  const [preload, setPreload] = useState(false);
  const user = useSelector((state) => state.User.Data);

  const [poster, setPoster] = useState();
  const [posterId, setPosterId] = useState(null);

  const [video, setVideo] = useState();
  const [videoId, setVideoId] = useState(null);

  const [durationCourse, setDurationCourse] = useState();

  const [titleCourse, setTitleCourse] = useState("");
  const [titleEnCourse, setTitleEnCourse] = useState("");

  const [lable, setLabel] = useState("");

  const [priceCourse, setPriceCourse] = useState("");

  const [descriptionCourse, setDescriptionCourse] = useState("");
  const [descriptionEnCourse, setDescriptionEnCourse] = useState("");

  const AddCourseAction = async () => {
    if (
      titleCourse === "" ||
      titleEnCourse === "" ||
      priceCourse === "" ||
      descriptionCourse === "" ||
      descriptionEnCourse === "" ||
      posterId === null ||
      videoId === null ||
      durationCourse === ""
    ) {
      toast.error("فیلد ها خالی هستند.", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      var raw = JSON.stringify({
        id: 0,
        title: titleCourse,
        titleen: titleEnCourse,
        price: priceCourse,
        description: descriptionCourse,
        descriptionen: descriptionEnCourse,
        offcodeid: 0,
        havecertificate: true,
        previewimageid: posterId,
        previewvideoid: videoId,
        userid: user.id,
        duration: durationCourse,
        lable: lable,
      });
      setPreload(true);

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      try {
        console.log(JSON.parse(raw));
        const result = await CallApi(
          BaseUrl + ADD_COURSE,
          raw,
          myHeaders,
          "POST"
        );

        console.log(result);
        if (result[1] === 200) {
          setPreload(false);
          toast.success("با موفقیت ثبت شد", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });

          window.location.replace(
            "/panel/courses/edit?id=" + result[0].data.id
          );
        } else {
          setPreload(false);
          toast.error("خطای سیستمی", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      } catch {
        setPreload(false);
        toast.error("خطای سیستمی", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    }
  };

  const changeState = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case "title":
        setTitleCourse(value);
        break;
      case "titleEn":
        setTitleEnCourse(value);
        break;
      case "price":
        setPriceCourse(value);
        break;
      case "description":
        setDescriptionCourse(value);
        break;
      case "descriptionEn":
        setDescriptionEnCourse(value);
        break;
      case "duration":
        setDurationCourse(value);
        break;
      case "lable":
        setLabel(value);
        break;
      default:
        break;
    }
  };

  const setImagePoster = async (e) => {
    setPreload(true);
    var myHeaders = new Headers();
    // myHeaders.append("Content-Type", "application/json");

    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);

    try {
      const result = await CallApi(
        BaseUrl + UPLOAD_IMAGE,
        formdata,
        myHeaders,
        "POST"
      );
      console.log(result);
      if (result[1] === 200) {
        let id = result[0].data.id;
        setPosterId(id);
        /*     element.src=result[0].data.filePath; */
        let src = result[0].data.filePath;
        setPoster(src);
        setPreload(false);
        toast.success("عکس با موفقیت ثبت شد", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        setPoster(false);
        toast.error("خطا در آپلود عکس", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setPoster(false);
      toast.error("خطا در آپلود عکس", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  const setVideoCourse = async (e) => {
    setPreload(true);
    var myHeaders = new Headers();
    // myHeaders.append("Content-Type", "application/json");

    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);

    try {
      const result = await CallApi(
        BaseUrl + UPLOAD_VIDEO,
        formdata,
        myHeaders,
        "POST"
      );
      console.log(result);
      if (result[1] === 200) {
        let id = result[0].data.id;
        setVideoId(id);
        /*     element.src=result[0].data.filePath; */
        let src = result[0].data.filePath;
        setVideo(src);
        setPreload(false);
        toast.success("ویدیو با موفقیت ثبت شد", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        setPoster(false);
        toast.error("خطا در آپلود ویدیو", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setPoster(false);
      toast.error("خطا در آپلود ویدیو", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  return (
    <>
      <Header title="افزودن دوره" />
      {preload ? (
        <Loading />
      ) : (
        <Box
          table={true}
          child={
            <>
              <div className="flex-center center-row rtl">
                <UploadAsset
                  src={poster}
                  title="عکس دوره"
                  accept="image/*"
                  id="PrimaryImage"
                  Change={setImagePoster}
                />
              </div>
              <div className="flex-center center-row rtl">
                <UploadAsset
                  src={video}
                  title="پیش نمایش دوره"
                  accept="video/*"
                  id="previewvideoid"
                  video={true}
                  Change={setVideoCourse}
                />
              </div>
              <div className="row-fields">
                <TextField
                  value={titleCourse}
                  required
                  label="عنوان دوره"
                  name="title"
                  onChange={changeState}
                />
                <TextField
                  value={titleEnCourse}
                  required
                  label="عنوان انگلیسی دوره "
                  name="titleEn"
                  onChange={changeState}
                />
                <TextField
                  value={priceCourse}
                  required
                  label="قیمت دوره "
                  name="price"
                  onChange={changeState}
                />
                <TextField
                  value={durationCourse}
                  required
                  label="مدت زمان دوره "
                  name="duration"
                  onChange={changeState}
                />
                <TextField
                  value={lable}
                  required
                  label="برچسب دوره "
                  name="lable"
                  onChange={changeState}
                />
                {/* <TextField
                value={ProductLable}
                onChange={(e) => ChangeLable(e)}
                label="عنوان دسته بندی"
              />
              <TextField
                required
                value={Weight}
                label="وزن"
                type="text"
                className="ltr-input"
                onChange={(e) => ChangeWeight(e)}
              />

              <TextField
                required
                value={Material}
                label="محتویات"
                type="text"
                className="ltr-input"
                onChange={(e) => ChangeMaterial(e)}
              /> */}
              </div>

              <div className="row-fields center-row">
                <TextareaAutosize
                  value={descriptionCourse}
                  labelId="Description"
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="توضیحات"
                  style={{ width: 480 }}
                  name="description"
                  onChange={changeState}
                />
                <TextareaAutosize
                  value={descriptionEnCourse}
                  labelId="DescriptionEn"
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="توضیحات انگلیسی"
                  style={{ width: 480 }}
                  name="descriptionEn"
                  onChange={changeState}
                />
              </div>
              <div className="row-fields"></div>
              <div className="row-fields">
                <Button onClick={AddCourseAction} variant="contained">
                  ثبت محصول{" "}
                </Button>
              </div>
              <ToastContainer />
            </>
          }
        />
      )}
    </>
  );
};

export default AddCourse;
