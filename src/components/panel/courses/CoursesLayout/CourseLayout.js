import React from "react";
import Container from "../../../LayoutContainers/Container";
import Nav from "../../../Navbar/Nav";
import AddCourse from "./AddCourse";
import UpdateCourse from "./UpdateCourse";

const CourseLayout = ({ action }) => {
  return (
    <>
      <Nav />
      <Container child={action === "add" ? <AddCourse /> : <UpdateCourse />} />
    </>
  );
};

export default CourseLayout;
