import React from "react";
import Container from "../../LayoutContainers/Container";
import Nav from "../../Navbar/Nav";
import CoursesContainer from "./CoursesContainer/CoursesContainer";
import { useSelector } from "react-redux";
import Accessdenied from "../../../tools/AccessDenied/Accessdenied";

const Courses = () => {
  const UserState = useSelector((state) => state.User);
  return (
    <>
      <Nav />
      <Container
        child={
          UserState.Data.userAccessLevel !== 3 ? (
            <CoursesContainer />
          ) : (
            <Accessdenied />
          )
        }
      />
    </>
  );
};

export default Courses;
