import { Button, FormControl, InputLabel, Link, TextareaAutosize } from "@material-ui/core";
import { MenuItem, Select, TextField } from "@material-ui/core";
import ColorPicker from "material-ui-color-picker";
import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import {
  BaseUrl,
  DeleteProductRequest,
  GetCollectionsRequest,
  GetProductRequest,
  UpdateProductRequest,
  UploadFileRequest,
} from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import Header from "../../../Header/Header";
import "./UpdateProduct.css";
import 'react-toastify/dist/ReactToastify.css';
import QuestionBox from "../../../../tools/QuestionBox/QuestionBox";
import UploadAsset from "../../../../tools/UploadImage/UploadAsset/UploadAsset";
import Asset from '../../../../images/logo.webp'
import { Checkbox, FormControlLabel, FormGroup } from "@mui/material";
const UpdateProduct = () => {
  const [product, setproduct] = useState();
  const [preload, setpreload] = useState(true);
  const [ProductId, setProductId] = useState(0);
  const [ProductTitle, setProductTitle] = useState('');
  const [ProductTitleEn, setProductTitleEn] = useState('');
  const [Weight, setWeight] = useState('');
  const [ProductPrice, setProductPrice] = useState('');
  const [ProductLable, setProductLable] = useState('');
  const [Material, setMaterial] = useState('');
  const [ProductDescription, setProductDescription] = useState('');
  const [ProductDescriptionEn, setProductDescriptionEn] = useState('');
  const [Imagesid, setImagesid] = useState([0, 0, 0, 0, 0, 0]);
  const [Images, setImages] = useState([Asset, Asset, Asset, Asset, Asset, Asset]);
  const [Popup, setPopup] = useState(Asset);
  useEffect(() => {
    const Loader = async () => {
      const search = window.location.search; // could be '?foo=bar'
      const params = new URLSearchParams(search);
      // bar
      var id = params.get("id");
      var d = await GetProduct(id);
      console.log(d);
      setProductId(d.id);
      setMaterial(d.material);
      setWeight(d.weigth);
      setProductPrice(d.price);
      setProductLable(d.lable);
      setProductTitle(d.title);
      setProductTitleEn(d.titleEn);

      if (d.description !== "string" && d.description.length > 0) {
        setProductDescription(d.description)
      }
      if (d.descriptionEn !== "string" && d.descriptionEn !== null) {
        setProductDescriptionEn(d.descriptionEn)
      }

      if (d.imageFile1 !== null) {
        Imagesid[0] = d.imageFile1.id;
        Images[0] = d.imageFile1.filePath;
      }
      if (d.imageFile2 !== null) {
        Imagesid[1] = d.imageFile2.id;
        Images[1] = d.imageFile2.filePath;
      }
      if (d.imageFile3 !== null) {
        Imagesid[2] = d.imageFile3.id;
        Images[2] = d.imageFile3.filePath;
      }
      if (d.imageFile4 !== null) {
        Imagesid[3] = d.imageFile4.id;
        Images[3] = d.imageFile4.filePath;
      }
      if (d.imageFile5 !== null) {
        Imagesid[4] = d.imageFile5.id;
        Images[4] = d.imageFile5.filePath;
      }
      if (d.imageFile6 !== null) {
        Imagesid[5] = d.imageFile6.id;
        Images[5] = d.imageFile6.filePath;
      }
      setImages(Images);
      setImagesid(Imagesid);
      setproduct(d);
      setpreload(false);
    };
    Loader();
  }, []);
  const CancelRemove = () => {
    setPopup(false);
  }
  const OpemPopUp = () => {
    setPopup(true);
  }
  const GetProduct = async (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    console.log(BaseUrl + GetProductRequest + "?ProductId=" + id);
    const result = await CallApi(
      BaseUrl + GetProductRequest + "?ProductId=" + id,
      raw,
      myHeaders,
      "GET"
    );
    console.log(result);
    if (result[1] === 200) {
      return result[0].data;
    }
  };
  const ChangeTitle = (e) => {
    setProductTitle(e.target.value);
  }
  const ChangeEnglishTitle = (e) => {
    setProductTitleEn(e.target.value);
  }

  const ChangeLable = (e) => {
    setProductLable(e.target.value);
  }
  const ChangeMaterial = (e) => {
    setMaterial(e.target.value);
  }
  const ChangePrice = (e) => {
    setProductPrice(e.target.value);
  }


  const ChangeDescription = (e) => {
    setProductDescription(e.target.value);
  }
  const ChangeDescriptionEn = (e) => {
    setProductDescriptionEn(e.target.value);
  }
  const ChangeWeight = (e) => {
    setWeight(e.target.value);
  }


  const UpdateProductAction = async () => {
    setpreload(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({
      "id": ProductId,
      "title": ProductTitle,
      "titleen": ProductTitleEn,
      "price": ProductPrice,
      "description": ProductDescription,
      "descriptionen": ProductDescriptionEn,
      "image1": Imagesid[0],
      "image2": Imagesid[1],
      "image3": Imagesid[2],
      "image4": Imagesid[3],
      "image5": Imagesid[4],
      "image6": Imagesid[5],
    });
    const result = await CallApi(
      BaseUrl + UpdateProductRequest,
      raw,
      myHeaders,
      "POST"
    );





    if (result[1] === 200) {
      /*    dispatch(Success(result[0].data)); */
      setpreload(false);
      toast.success('با موفقیت ثبت شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

    }
    else {
      toast.error('خطا در انجام عملیات', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  const RemoveProduct = async () => {
    setpreload(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    const result = await CallApi(
      BaseUrl + DeleteProductRequest + "?id=" + ProductId,
      raw,
      myHeaders,
      "POST"
    );
    if (result[1] === 200) {
      setpreload(false);
      setPopup(false);
      toast.success('با موفقیت حذف شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      /* const history = unstable_HistoryRouter();
      history.push("/panel/products"); */
      window.location.replace("/panel/products");
    }
    else {
      setpreload(false);
      setPopup(false);
      toast.error('خطا در انجام عملیات', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }
  const ChangeImage = async (e, id, index) => {
    setpreload(true);
    var myHeaders = new Headers();
    /*  myHeaders.append("Content-Type", "application/json"); */

    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);


    try {
      const result = await CallApi(
        BaseUrl + UploadFileRequest,
        formdata,
        myHeaders,
        "POST"
      );
      /* console.log(result) */
      if (result[1] === 200) {
        console.log(result[0].data);
        Imagesid[index] = result[0].data.id;
        setImagesid(Imagesid);
        console.log(Imagesid);
        console.log(result[0].data);
        /*     element.src=result[0].data.filePath; */
        Images[index] = result[0].data.filePath;
        setpreload(false);
        setImages(Images);
        toast.success('محصول با موفقیت ثبت شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      else {
        setpreload(false);
        toast.error('خطا در آپلود عکس', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setpreload(false);
      toast.error('خطا در آپلود عکس', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }

  const RemovePic = (PicId) => {
    Imagesid[PicId] = 0;
    setImagesid(Imagesid);
    Images[PicId] = Asset;
    setImages(Images);
    UpdateProductAction();
  }
  return (
    <>
      <Header title="به روز رسانی محصول" />
      {preload === true ? (
        <Loading />
      ) : (
        <Box
          table={true}
          child={
            <>
              <div className="flex-center center-row rtl">
                <UploadAsset src={Images[0]} title="عکس اصلی" accept="image/*" id="PrimaryImage" Change={(e) => ChangeImage(e, "PrimaryImage", 0)} />
              </div>
              <div className="flex-center center-row rtl">
                <UploadAsset src={Images[1]} title="عکس اول" accept="image/*" id="Image1" Change={(e) => ChangeImage(e, "Image1", 1)} />
                <UploadAsset src={Images[2]} title="عکس دوم" accept="image/*" id="Image2" Change={(e) => ChangeImage(e, "Image2", 2)} />
                <UploadAsset src={Images[3]} title="عکس سوم" accept="image/*" id="Image3" Change={(e) => ChangeImage(e, "Image3", 3)} />
                <UploadAsset src={Images[4]} title="عکس چهارم" accept="image/*" id="Image4" Change={(e) => ChangeImage(e, "Image4", 4)} />
                <UploadAsset src={Images[5]} title="عکس پنجم" accept="image/*" id="Image5" Change={(e) => ChangeImage(e, "Image5", 5)} />
              </div>

              <div className="row-fields">
                <TextField
                  value={ProductTitle}
                  required
                  label="عنوان محصول "
                  onChange={(e) => ChangeTitle(e)}
                />
                <TextField
                  value={ProductTitleEn}
                  required
                  label="عنوان انگلیسی محصول "
                  onChange={(e) => ChangeEnglishTitle(e)}
                />
                <TextField value={ProductPrice} required label="قیمت محصول " onChange={(e) => ChangePrice(e)} />
                <TextField
                  value={ProductLable}
                  onChange={(e) => ChangeLable(e)}
                  label="عنوان دسته بندی"
                />
          
              </div>

              <div className="row-fields center-row">
                <TextareaAutosize
                  value={ProductDescription}
                  labelId="Description"
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="توضیحات"
                  style={{ width: 480 }}
                  onChange={(e) => ChangeDescription(e)}
                />
                <TextareaAutosize
                  value={ProductDescriptionEn}
                  labelId="DescriptionEn"
                  aria-label="minimum height"
                  minRows={5}
                  placeholder="توضیحات انگلیسی"
                  style={{ width: 480 }}
                  onChange={(e) => ChangeDescriptionEn(e)}
                />
              </div>
              <div className="row-fields">

              </div>
              <div className="row-fields">
                <Button onClick={UpdateProductAction} variant="contained">به روز رسانی محصول </Button>

              </div>
              <ToastContainer />


            </>
          }

        />
      )}
    </>
  );
};

export default UpdateProduct;
