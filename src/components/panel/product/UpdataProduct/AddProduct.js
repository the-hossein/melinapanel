import { Button, TextareaAutosize, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import { BaseUrl, CreateProductRequest, UploadFileRequest } from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import UploadAsset from "../../../../tools/UploadImage/UploadAsset/UploadAsset";
import Header from "../../../Header/Header";
import Asset from '../../../../images/logo.webp'
const AddProduct = () => {

  const [preload, setpreload] = useState(true);
  const [ProductTitle, setProductTitle] = useState('');
  const [ProductTitleEn, setProductTitleEn] = useState('');
  const [Weight, setWeight] = useState('');
  const [ProductPrice, setProductPrice] = useState('');
  const [ProductLable, setProductLable] = useState('');
  const [Material, setMaterial] = useState('');
  const [ProductDescription, setProductDescription] = useState('');
  const [ProductDescriptionEn, setProductDescriptionEn] = useState('');
  const [Imagesid, setImagesid] = useState([0, 0, 0, 0, 0, 0]);
  const [Images, setImages] = useState([Asset, Asset, Asset, Asset, Asset, Asset]);
  useEffect(() => {
    const GetData = async () => {
      setpreload(false);
    }
    GetData()
  }, []);
  const ChangeTitle = (e) => {
    setProductTitle(e.target.value);
  }
  const ChangeEnglishTitle = (e) => {
    setProductTitleEn(e.target.value);
  }

  const ChangeLable = (e) => {
    setProductLable(e.target.value);
  }
  const ChangeMaterial = (e) => {
    setMaterial(e.target.value);
  }
  const ChangePrice = (e) => {
    setProductPrice(e.target.value);
  }


  const ChangeDescription = (e) => {
    setProductDescription(e.target.value);
  }
  const ChangeDescriptionEn = (e) => {
    setProductDescriptionEn(e.target.value);
  }
  const ChangeWeight = (e) => {
    setWeight(e.target.value);
  }

  const AddProductAction = async () => {
    if (ProductTitle.length > 0 && ProductPrice.length > 0 > 0 && Imagesid[0] > 0) {
      setpreload(true);

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        "title": ProductTitle,
        "titleen": ProductTitleEn,
        "price": ProductPrice,
        "description": ProductDescription,
        "descriptionen": ProductDescriptionEn,
        "offcodeid": 0,
        "image1": Imagesid[0],
        "image2": Imagesid[1],
        "image3": Imagesid[2],
        "image4": Imagesid[3],
        "image5": Imagesid[4],
        "image6": Imagesid[5],
        "material": Material,
        "weigth": Weight,
        "lable": ProductLable,
      });
      console.log(raw);
      const result = await CallApi(
        BaseUrl + CreateProductRequest,
        raw,
        myHeaders,
        "POST"
      );
      console.log(result);
      if (result[1] === 200) {
        setpreload(false);
        /*    dispatch(Success(result[0].data)); */
        toast.success('محصول با موفقیت ثبت شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });

        window.location.replace("/panel/products/edit?id=" + result[0].data.id);
      }
      else {
        toast.error('خطا در ثبت محصول', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setpreload(false);
      }
    }
    else {
      toast.error('فیلد های خواسته شده را وارد کنید', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }


  }
  const ChangeImage = async (e, id, index) => {
    var element = document.getElementById(id);
    setpreload(true);
    var myHeaders = new Headers();
    /*  myHeaders.append("Content-Type", "application/json"); */

    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);


    try {
      const result = await CallApi(
        BaseUrl + UploadFileRequest,
        formdata,
        myHeaders,
        "POST"
      );
      /* console.log(result) */
      if (result[1] === 200) {
        Imagesid[index] = result[0].data.id;
        setImagesid(Imagesid);
        /*     element.src=result[0].data.filePath; */
        Images[index] = result[0].data.filePath;
        setpreload(false);
        setImages(Images);
        toast.success('عکس با موفقیت ثبت شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      else {
        setpreload(false);
        toast.error('خطا در آپلود عکس', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setpreload(false);
      toast.error('خطا در آپلود عکس', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }

  return <>
    <Header title="افزودن محصول" />
    {preload === true ? <Loading /> :
      <Box
        table={true}
        child={
          <>
            <div className="flex-center center-row rtl">
              <UploadAsset src={Images[0]} title="عکس اصلی" accept="image/*" id="PrimaryImage" Change={(e) => ChangeImage(e, "PrimaryImage", 0)} />
            </div>
            <div className="flex-center center-row rtl">
              <UploadAsset src={Images[1]} title="عکس اول" accept="image/*" id="Image1" Change={(e) => ChangeImage(e, "Image1", 1)} />
              <UploadAsset src={Images[2]} title="عکس دوم" accept="image/*" id="Image2" Change={(e) => ChangeImage(e, "Image2", 2)} />
              <UploadAsset src={Images[3]} title="عکس سوم" accept="image/*" id="Image3" Change={(e) => ChangeImage(e, "Image3", 3)} />
              <UploadAsset src={Images[4]} title="عکس چهارم" accept="image/*" id="Image4" Change={(e) => ChangeImage(e, "Image4", 4)} />
              <UploadAsset src={Images[5]} title="عکس پنجم" accept="image/*" id="Image5" Change={(e) => ChangeImage(e, "Image5", 5)} />
            </div>
            <div className="row-fields">
              <TextField
                value={ProductTitle}
                required
                label="عنوان محصول "
                onChange={(e) => ChangeTitle(e)}
              />
              <TextField
                value={ProductTitleEn}
                required
                label="عنوان انگلیسی محصول "
                onChange={(e) => ChangeEnglishTitle(e)}
              />
              <TextField value={ProductPrice} required label="قیمت محصول " onChange={(e) => ChangePrice(e)} />
              <TextField
                value={ProductLable}
                onChange={(e) => ChangeLable(e)}
                label="عنوان دسته بندی"
              />
              {/*     <TextField
                required
                value={Weight}
                label="وزن"
                type="text"
                className="ltr-input"
                onChange={(e) => ChangeWeight(e)}
              />

              <TextField
                required
                value={Material}
                label="محتویات"
                type="text"
                className="ltr-input"
                onChange={(e) => ChangeMaterial(e)}
              /> */}
            </div>

            <div className="row-fields center-row">
              <TextareaAutosize
                value={ProductDescription}
                labelId="Description"
                aria-label="minimum height"
                minRows={5}
                placeholder="توضیحات"
                style={{ width: 480 }}
                onChange={(e) => ChangeDescription(e)}
              />
              <TextareaAutosize
                value={ProductDescriptionEn}
                labelId="DescriptionEn"
                aria-label="minimum height"
                minRows={5}
                placeholder="توضیحات انگلیسی"
                style={{ width: 480 }}
                onChange={(e) => ChangeDescriptionEn(e)}
              />
            </div>
            <div className="row-fields">

            </div>
            <div className="row-fields">
              <Button onClick={AddProductAction} variant="contained">ثبت محصول </Button>

            </div>
            <ToastContainer />


          </>
        }

      />
    }
  </>;
};

export default AddProduct;
