import { Button, TextareaAutosize, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import { BaseUrl, CreateCollectionRequest, UploadFileRequest } from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import UploadAsset from "../../../../tools/UploadImage/UploadAsset/UploadAsset";
import Header from "../../../Header/Header";
import Asset from '../../../../images/logo.webp'
const AddCollection = () => {
  const [preload, setpreload] = useState(false);

  const [CollectionTitle, setCollectionTitle] = useState('');
  const [CollectionColor, setCollectionColor] = useState('');
  const [CollectionTitleEn, setCollectionTitleEn] = useState('');
  const [Image, setImage] = useState(Asset);
  const [Imagesid, setImagesid] = useState();
  const [CollectionDescription, setCollectionDescription] = useState('');
  const [CollectionDescriptionEn, setCollectionDescriptionEn] = useState('');
  const ChangeImage = async (e) => {
    setpreload(true);
    var myHeaders = new Headers();
    /*  myHeaders.append("Content-Type", "application/json"); */

    var formdata = new FormData();
    formdata.append("File", e.target.files[0], e.target.files[0].name);


    try {
      const result = await CallApi(
        BaseUrl + UploadFileRequest,
        formdata,
        myHeaders,
        "POST"
      );
      /* console.log(result) */
      if (result[1] === 200) {
        setImagesid(result[0].data.id);
        setpreload(false);
        setImage(result[0].data.filePath);
        toast.success('عکس با موفقیت ثبت شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
      else {
        setpreload(false);
        toast.error('خطا در آپلود عکس', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    } catch {
      setpreload(false);
      toast.error('خطا در آپلود عکس', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

  }
  const ChangeDescription = (e) => {
    setCollectionDescription(e.target.value)
  }
  const ChangeDescriptionEn = (e) => {
    setCollectionDescriptionEn(e.target.value)
  }
  const ChangeTitle = (e) => {
    setCollectionTitle(e.target.value)
  }
  const ChangeTitleEn = (e) => {
    setCollectionTitleEn(e.target.value)
  }
  const ChangeColor = (e) => {
    setCollectionColor(e.target.value)
  }
  const AddCollectionAction=async()=>{
    if (CollectionTitle.length > 0 && CollectionTitleEn.length > 0 && Imagesid > 0 ) {
      setpreload(true);
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      var raw = JSON.stringify({
          "title": CollectionTitle,
          "titleen": CollectionTitleEn,
          "colorcode": CollectionColor,
          "picid": Imagesid,
          "description": CollectionDescription,
          "descriptionen": CollectionDescriptionEn
      });
      const result = await CallApi(
        BaseUrl + CreateCollectionRequest,
        raw,
        myHeaders,
        "POST"
      );
      if (result[1] === 200) {
        setpreload(false);
        /*    dispatch(Success(result[0].data)); */
        toast.success('محصول با موفقیت ثبت شد', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
     
        window.location.replace("/panel/collection/edit?id=" + result[0].data.id);
      }
      else {
        toast.error('خطا در ثبت محصول', {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        setpreload(false);
      }
    }
    else {
      toast.error('فیلد های خواسته شده را وارد کنید', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }

  return  <>
  <Header title="اضافه کردن کالکشن" />
  {preload === true ? (
    <Loading />
  ) : (
    <Box
      child={
        <>
          <div className="flex-center center-row rtl">
            <UploadAsset src={Image} title="عکس اصلی" accept="image/*" Change={(e) => ChangeImage(e)} />

          </div>
          <div className="row-fields">
            <TextField
              value={CollectionTitle}
              required
              label="عنوان کالکشن "
              onChange={(e) => ChangeTitle(e)}
            />
            <TextField
              value={CollectionTitleEn}
              required
              label="عنوان انگلیسی کالکشن "
              onChange={(e) => ChangeTitleEn(e)}
            />
            <TextField
              value={CollectionColor}
              required
              label="کد رنگ "
              className="ltr-input"
              onChange={(e) => ChangeColor(e)}
            />
          </div>
          <div className="row-fields center-row">
            <TextareaAutosize
              value={CollectionDescription}
              labelId="Description"
              aria-label="minimum height"
              minRows={5}
              placeholder="توضیحات"
              style={{ width: 480 }}
              onChange={(e) => ChangeDescription(e)}
            />
            <TextareaAutosize
              value={CollectionDescriptionEn}
              labelId="Description"
              aria-label="minimum height"
              minRows={5}
              placeholder="توضیحات انگلیسی"
              style={{ width: 480 }}
              onChange={(e) => ChangeDescriptionEn(e)}
            />
          </div>
          <div className="row-fields">
            <Button onClick={AddCollectionAction} variant="contained">به روز رسانی</Button>
           {/*  <Button onClick={OpemPopUp} className="error-btn" variant="outlined" color="error">
              حذف محصول
            </Button> */}
          </div>
        </>
      }
    />
  )}
  <ToastContainer />
</>;
};

export default AddCollection;
