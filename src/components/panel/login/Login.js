import { Box, Button, TextField } from '@material-ui/core'
import style from './Login.module.css'
import React, { useEffect, useState } from 'react'
import { CallApi } from '../../../tools/CallApi/CallApi';
import { BaseUrl, LoginReqest, SignUpReqest } from '../../../api/Url';
import { toast, ToastContainer } from 'react-toastify';
import Loading from '../../../tools/Loading/Loading';
import { useDispatch, useSelector } from 'react-redux';
import { GetUserData } from '../../../redux/User/UserAction';
import Logo from "../../../images/logo.webp";
const Login = () => {
    const UserState = useSelector((state) => state.User);
    const dispath = useDispatch();
    const [PhoneNumber, setPhoneNumber] = useState('');
    const [Code, setCode] = useState('');
    const [Password, setPassword] = useState('');
    const [StepNumber, setStepNumber] = useState(1);
    const [preload, setpreload] = useState(true);
    const [wait, setWait] = useState(false)

    useEffect(() => {

        if (localStorage.getItem("UserToken")) {
            var token = JSON.parse(localStorage.getItem("UserToken"))
            const now = new Date();
            const endDate = new Date(token.expiration);
            if (endDate - now < 0) {
                localStorage.removeItem("UserToken");
                localStorage.removeItem("UserPhoneNumber");

                if (window.location.pathname !== "/") {
                    window.location = "/"
                }
                else {
                    setpreload(false)
                }


            } else {

                window.location = "/panel/dashboard";
            }

        } else {

            /*   console.log(window.location.pathname); */
            if (window.location.pathname !== "/") {
                window.location = "/"
            }
            else {
                setpreload(false)
            }
        }

    }, []);
    const ChangePhoneNumber = (e) => {
        setPhoneNumber(e.target.value)
    }
    const ChangeCode = (e) => {
        setCode(e.target.value)
    }
    const ChangePassword = (e) => {
        setPassword(e.target.value)
    }
    const LoginAction = async (e) => {
        setpreload(true)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "methodname": 5,
            "checkPhoneNumber": {
                "phonenumber": PhoneNumber
            }
        });

        const result = await CallApi(
            BaseUrl + SignUpReqest,
            raw,
            myHeaders,
            "POST"
        );
        if (result[1] === 200) {
            setpreload(false)
            setStepNumber(2);
            toast.success('کد تایید برای شما ارسال گردید', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        else {
            setpreload(false)
            toast.error('خطا در انجام عملیات', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }
    const SetCode = async () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "methodname": 2,
            "otpCheck": {
                "phoneNumber": PhoneNumber,
                "code": Code,
                "referralSite": "string",
                "type": 0
            }
        });

        const result = await CallApi(
            BaseUrl + SignUpReqest,
            raw,
            myHeaders,
            "POST"
        );
        if (result[1] === 200) {

            localStorage.setItem("UserToken", JSON.stringify(result[0].data));
            localStorage.setItem("UserPhoneNumber", PhoneNumber);
            window.location = "/panel/dashboard"
        }
        else {
            let num;
            if (localStorage.getItem("tryWrong") === null) {
                num = 1;
                localStorage.setItem("tryWrong", JSON.stringify(num));
            } else {
                num = JSON.parse(localStorage.getItem("tryWrong"));
                num++;
                localStorage.setItem("tryWrong", JSON.stringify(num));
            }
            let btryClient = window.localStorage.getItem("tryWrong");

            if (JSON.parse(btryClient) >= 5) {

                toast.error('شما بیش از حد کد را وارد کرده اید', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

                setWait(true);

                if (window.localStorage.getItem("setWrong") === null) {
                    let num = 1;
                    window.localStorage.setItem("setWrong", JSON.stringify(num));
                } else {
                    let num = JSON.parse(window.localStorage.getItem("setWrong"));
                    num++;
                    window.localStorage.setItem("setWrong", JSON.stringify(num));
                }

                setTimeout(() => {
                    window.localStorage.removeItem("tryWrong");
                    setWait(false);
                }, 60000);

            }
            toast.error('خطا در انجام عملیات', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }
    const errorWaitCode = () => {
        toast.error('لطفا صبر کنید شما بیش از حد مجاز کد اشتباه وارد کرده اید', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
    /*   const LoginWithPasswordAction = async () => {
          setpreload(true)
          var myHeaders = new Headers();
          myHeaders.append("Content-Type", "application/json");
  
          var raw = JSON.stringify({
              "methodname": 3,
              "loginWithPassword": {
                  "userName": PhoneNumber,
                  "password": Password,
                  "rememberMe": true,
                  "returnUrl": "string"
              }
  
          });
          const result = await CallApi(
              BaseUrl + LoginReqest,
              raw,
              myHeaders,
              "POST"
          );
          console.log(result);
          if (result[1] === 200) {
              setpreload(false)
              toast.success('با موفقییت وارد شدید', {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
              });
          
          }
          else {
              setpreload(false)
              toast.error('خطا در انجام عملیات', {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
              });
          }
      } */
    return (
        <>
            {preload === true ? <Loading /> : StepNumber === 2 ?
                <div className={style.Loginbox}>
                    <Box>
                        <img src={Logo} />
                        <TextField value={Code} type="text" placeholder='Enter Verify Code' onChange={(e) => ChangeCode(e)} />
                        {/*  <TextField type="password" placeholder='Enter Password'/> */}
                        <Button color="primary" onClick={wait ? errorWaitCode : SetCode}>ثبت کد</Button>
                    </Box>
                </div>
                :

                <div className={style.Loginbox}>
                    <Box>
                        <img src={Logo} />
                        <TextField value={PhoneNumber} type="text" placeholder='Enter PhoneNumber' onChange={(e) => ChangePhoneNumber(e)} />
                        {/*  <TextField type="password" placeholder='Enter Password'/> */}
                        <Button color="primary" onClick={LoginAction}>ورود</Button>
                    </Box>
                </div>
            }
            <ToastContainer />

        </>
    )
}

export default Login