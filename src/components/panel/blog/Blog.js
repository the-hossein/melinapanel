import React from "react";
import { useSelector } from "react-redux";
import Accessdenied from "../../../tools/AccessDenied/Accessdenied";
import Container from "../../LayoutContainers/Container";
import Nav from "../../Navbar/Nav";
import BlogContainer from "./blogContainer/BlogContainer";


const Blog = () => {
  const UserState = useSelector((state) => state.User)
  return (
    <>
      <Nav />
      <Container child={UserState.Data.accessLevel !== 3 && UserState.Data.accessLevel !== 1 ? <BlogContainer /> : <Accessdenied/>} />
    </>
  );
};

export default Blog;
