import {  FormControl, InputLabel, Link, TextareaAutosize } from "@material-ui/core";
import Button from '@mui/material/Button';
import { MenuItem, Select, TextField } from "@material-ui/core";
import ColorPicker from "material-ui-color-picker";
import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import {
  BaseUrl,
  ConfirmeCommnetRequest,
  DeleteProductRequest,
  GetCollectionsRequest,
  GetCommnetRequest,
  GetProductRequest,
  UpdateProductRequest,
  UploadFileRequest,
} from "../../../../api/Url";
import Box from "../../../../tools/box/Box";
import { CallApi } from "../../../../tools/CallApi/CallApi";
import Loading from "../../../../tools/Loading/Loading";
import Header from "../../../Header/Header";
import "./UpdateProduct.css";
import 'react-toastify/dist/ReactToastify.css';
import QuestionBox from "../../../../tools/QuestionBox/QuestionBox";
import UploadAsset from "../../../../tools/UploadImage/UploadAsset/UploadAsset";
import Asset from '../../../../images/logo.webp'
import { PersionDate } from "../../../../tools/Tools";
const UpdateComment = () => {

  const [preload, setpreload] = useState(true);
  const [Name, setName] = useState();
  const [id, setid] = useState();
  const [CreatDate, setCreatDate] = useState();
  const [Confirmed, setConfirmed] = useState();
  const [CommnetText, setCommnetText] = useState();

  useEffect(() => {
    const Loader = async () => {
      const search = window.location.search; // could be '?foo=bar'
      const params = new URLSearchParams(search);
      // bar
      var id = params.get("id");
      setid(id)
      var d = await GetComment(id);
      setName(d.user.name + " " + d.user.family);
      let Pd = await PersionDate(d.createdDatetime);
      setCreatDate(Pd);
      setCommnetText(d.commentText);
      setConfirmed(d.isConfirmed === true ? "تایید شده" : "عدم تایید")


      setpreload(false);
    };
    Loader();
  }, []);

  const GetComment = async (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});
    console.log(BaseUrl + GetProductRequest + "?id=" + id);
    const result = await CallApi(
      BaseUrl + GetCommnetRequest + "?CommentId=" + id,
      raw,
      myHeaders,
      "GET"
    );
    if (result[1] === 200) {
      return result[0].data;
    }
  };



  const UpdateComment = async (commentid) => {
    setpreload(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});

    const result = await CallApi(
      BaseUrl + ConfirmeCommnetRequest + "?id=" + commentid + "&Confirmed=true",
      raw,
      myHeaders,
      "POST"
    );
    if (result[1] === 200) {
      setpreload(false);
      setConfirmed("تایید شده");
      /*    dispatch(Success(result[0].data)); */
      toast.success('نظر با موفقیت ثبت شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    
    }
    else{
      setpreload(false);
      /*    dispatch(Success(result[0].data)); */
      toast.error('خطا در ثبت نظر', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }
  const UnConfirmedComment = async (commentid) => {
    setpreload(true);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({});

    const result = await CallApi(
      BaseUrl + ConfirmeCommnetRequest + "?id=" + commentid + "&Confirmed=false",
      raw,
      myHeaders,
      "POST"
    );
    if (result[1] === 200) {
    
      /*    dispatch(Success(result[0].data)); */
      toast.success('نظر با موفقیت ثبت شد', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      setpreload(false);
      setConfirmed("عدم تایید");
    }
    else{
      setpreload(false);
      /*    dispatch(Success(result[0].data)); */
      toast.error('خطا در ثبت نظر', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  }
  return (
    <>
      <Header title="به روز رسانی محصول" />
      {preload === true ? (
        <Loading />
      ) : (
        <Box
          child={
            <>

              <div className="row-fields">
                <TextField
                  value={Name}
                  required
                  label="نام "

                />
                <TextField
                  value={CreatDate}
                  required
                  label="تاریخ"

                />
                <TextField
                  value={Confirmed}
                
                  
                  required
                  label="وضعیت"

                />

              </div>
              <div className="row-fields">

                <TextareaAutosize
                  value={CommnetText}
                  labelId="Description"
                  aria-label="minimum height"
                  minRows={5}
                 
                  style={{ width: 480 }}

                />
              </div>
             
              <div className="row-fields">
                <Button onClick={(e)=>UpdateComment(id)} variant="contained">تایید نظر</Button>
                <Button onClick={(e)=>UnConfirmedComment(id)} variant="contained" color="error">عدم تایید</Button>
              </div>
              <ToastContainer />


            </>
          }
        />
      )}
    </>
  );
};

export default UpdateComment;
