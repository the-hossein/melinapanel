import React from "react";
import { useSelector } from "react-redux";
import Accessdenied from "../../../../tools/AccessDenied/Accessdenied";
import Container from "../../../LayoutContainers/Container";

import Nav from "../../../Navbar/Nav";

import UpdateComment from "./UpdateComments";

const CommentLayout = (props) => {
  const UserState = useSelector((state) => state.User)
  return (
    <>
      {UserState.Data.accessLevel !== 3 && UserState.Data.accessLevel !== 2 && UserState.Data.accessLevel !== 1 ?
        <>
          <Nav />
          <Container
            child={<UpdateComment />}
          /> </>
        : <Accessdenied />

      }

    </>
  );
};

export default CommentLayout;
