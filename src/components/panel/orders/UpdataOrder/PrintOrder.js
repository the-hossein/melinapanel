import React from "react";
import QRCode from "react-qr-code";
import Header from "../../../Header/Header";

import UpdateOrder from "./UpdateOrder";
import { PersionCurrency } from "../../../../tools/Tools";
export class PrintOrder extends React.PureComponent {

    render() {
        return (
            <>
           
                <div className="factore-data">
                    <h2>{`کد فاکتور : ${this.props.factore.id}`}</h2>
                    <h3>{`نام مشتری : ${this.props.factore.user.name} ${this.props.factore.user.family}`}</h3>
                    <p>{`شماره تماس : ${this.props.factore.phoneNumber}`}</p>
                    <p>{`آدرس : ${this.props.factore.address.address}`}</p>
                    <p>{`روش ارسال  : ${this.props.factore.shiping === 0 ? "پست پیشتاز (20,000 تومان)" : "پیک سفارشی"}`}</p>
                    <p>{`کد پستی : ${this.props.factore.address.postCode}`}</p>
                    {this.props.factore.state === 0 ? null : <p>{`وضعیت سفارش : ${this.props.factore.state === 4 ? "در حال بسته بندی" : this.props.factore.state === 5 ? "ارسال برای مشتری" : this.props.factore.state === 6 ? "سفارش بسته شده است" : "سفارش تایید نشده"} `}</p>}
                    {this.props.factore.offCode !==null ?<p>کد تخفیف :  {PersionCurrency(this.props.factore.offCode.price)}  تومنی</p> : null}
                    <div className='qrcode'>
                        <QRCode
                            title="Tiola"
                            value={this.props.qrlink}
                            bgColor="#FFFFFF"
                            fgColor="#000000"
                            size={128}
                        />
                    </div>
                </div>

                {/*   <UpdateOrder /> */}
            </>

        );
    }
}
