import React from "react";
import { useSelector } from "react-redux";
import Accessdenied from "../../../tools/AccessDenied/Accessdenied";
import Container from "../../LayoutContainers/Container";
import Nav from "../../Navbar/Nav";
import OrderContainer from "./OrderContainer/OrderContainer";


const Order = (props) => {
  const UserState = useSelector((state) => state.User)
  return (
    <>
      <Nav />
      <Container child={UserState.Data.accessLevel !== 3 ? <OrderContainer state={props.state}/> : <Accessdenied/>} />
    </>
  );
};

export default Order;
