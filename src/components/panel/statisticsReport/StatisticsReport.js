import React from "react";
import { useSelector } from "react-redux";
import Container from "../../LayoutContainers/Container";
import Nav from "../../Navbar/Nav";
import StatisticsContainer from "./statistcsContainer/StatisticsContainer";


const StatisticsReport = () => {

  const UserState = useSelector((state) => state.User)

  return (
    <>
      <Nav />
      <Container child={UserState.Data.accessLevel === 0 && <StatisticsContainer />} />
    </>
  );
};

export default StatisticsReport;
