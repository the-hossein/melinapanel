import React from "react";
import Container from "../../LayoutContainers/Container";
import Nav from "../../Navbar/Nav";
import Body from "./dashboardBody/Body";

const Dashboard = () => {
  return (
    <>
      <Nav />
      <Container child={<Body />} />
    </>
  );
};

export default Dashboard;
