import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import "./Nav.css";
import DashboardIcon from "@material-ui/icons/Dashboard";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { useDispatch, useSelector } from "react-redux";
import Logo from '../../images/logo.webp'
import { Style } from "@material-ui/icons";
import OndemandVideoIcon from '@mui/icons-material/OndemandVideo';

const Nav = () => {
  const dispath = useDispatch();
  const UserState = useSelector((state) => state.User)
  const [preload, setpreload] = useState(true);
  const [Count, setCount] = useState(true);
  useEffect(() => {
  }, []);

  return (
    <div className="navbar">
     {/*  <img className="meun_logo" src={Logo} /> */}
      <ul className="nav-menu">
        <li>
          <NavLink
            className={(isActive) =>
              isActive.isActive === true ? "active-nav-item" : ""
            }
            to="/panel/dashboard"
          >
            <DashboardIcon />
            <span>داشبورد</span>
          </NavLink>
        </li>
        <li>
          <NavLink
            className={(isActive) =>
              isActive.isActive === true ? "active-nav-item" : ""
            }
            to="/panel/products"
          >
            <ShoppingCartIcon />
            <span>محصولات</span>
          </NavLink>
        </li>
        <li>
          <NavLink
            className={(isActive) =>
              isActive.isActive === true ? "active-nav-item" : ""
            }
            to="/panel/courses"
          >
            <OndemandVideoIcon />
            <span>دوره ها</span>
          </NavLink>
        </li>
      </ul>

    </div>
  );
};

export default Nav;
