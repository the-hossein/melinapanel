import React from "react";
import "./Container.css";
import Logo from '../../images/logo.webp'
const Container = (props) => {
  return <div className="t-container">
    <img src={Logo} />
    {props.child}</div>;
};

export default Container;
