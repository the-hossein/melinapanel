import { applyMiddleware, createStore } from "redux";
import rootReducer from "./RootReducer";
import logger from "redux-logger";
import thunk from 'redux-thunk';
const Store = createStore(rootReducer, applyMiddleware(logger,thunk));

export default Store;
