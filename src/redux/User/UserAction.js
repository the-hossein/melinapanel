import { BaseUrl, GetUserDataRequest } from "../../api/Url";
import { CallApi } from "../../tools/CallApi/CallApi";

export const Request = () => {
  return { type: "Request" };
};

export const Success = (Data) => {
  return { type: "Success", payload: Data };
};
export const Faild = () => {
  return { type: "Faild" };
};

export const GetUserData = (value) => {
  return (dispath) => {
    dispath(Request());
   
    const GetData = async (PhoneNum) => {
      var myHeaders = new Headers();
      var token = JSON.parse(localStorage.getItem("UserToken"));
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", `Bearer ${token.token}`);
      var raw = JSON.stringify({
        "phonenumber": PhoneNum
      });
      
      const result = await CallApi(
        BaseUrl + GetUserDataRequest,
        raw,
        myHeaders,
        "POST"
      );
      if (result[1] === 200) {
        if(result[0].data.user.userAccessLevel === 1 || result[0].data.user.userAccessLevel === 2){
          dispath(Success(result[0].data.user))
        }else{
          if(window.location.pathname !== "/"){
            window.location = "/"
          }
        }
      }
      else {
        dispath(Faild())
      }
    }
    GetData(value);
  }
}

