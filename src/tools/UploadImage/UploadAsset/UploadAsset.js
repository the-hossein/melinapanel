import { IconButton } from "@material-ui/core";
import { PhotoCamera } from "@material-ui/icons";
import React, { } from "react";
import Asset from '../../../images/logo.webp'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import "./UploadAsset.css";
const UploadAsset = (props) => {
  return (
    <div className="edit-asset-item">
      {props.removeIcon !== false ? (
        <RemoveCircleOutlineIcon onClick={props.removeIcon} />
      ) : null}
      {props.video === true ? (
        <video autoPlay playsInline muted preload="true" loop className="video_player" >
          <source
            src={props.src}
            type="video/mp4"
            Accept-Ranges="bytes"
          />
          {/* <source src="movie.ogg" type="video/ogg"> */}
        </video>
      ) : (
        <img src={props.src} alt="" />
      )}
      <div>
        <div className="edit-img-profile mt-1">
          <input
            type="file"
            className="custom-file-input"
            onChange={props.Change}
            multiple
            accept={props.accept}
          />
          <IconButton
            color="primary"
            aria-label="upload picture"
            component="span"
          >
            <PhotoCamera />
          </IconButton>
        </div>
      </div>

      <p>{props.title}</p>
    </div>
  );
};

export default UploadAsset;
