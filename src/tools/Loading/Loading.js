import { CircularProgress } from "@material-ui/core";
import React from "react";
import gif from "../../images/loading2.gif";
import "./Loading.css";

const Loading = () => {
  return (
    <div className="loading-gif">
      {/*   <img src={gif} alt="loading" /> */}
      <CircularProgress />
    </div>
  );
};

export default Loading;
