import React from 'react';
import style from '../LineChart/LineChart.module.css';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';


ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

const VerticalChart = ({ reportProps, dataApi, labelsPro }) => {

    const options = {
        font:{
            family:"IRANSansWeb"
        },
        responsive: true,
      scales: {
        x: {
          grid: {
            display: false,
          },
        },
        y: {
          grid: {
            display: false,
          },
        },
      },
      plugins: {
          legend: {
              position: 'top' ,
              labels:{
                  usePointStyle: true,
                  font:{
                    size:12,
                    family: "IRANSansWeb"
                }
            }
        },
        title: {
          display: true,
          FontFace:"IRANSansWeb",
          text: "گزارش تعداد فروش شال ها",
          font:{
              size: 16,
              family: "IRANSansWeb"
          }
        },
        tooltip: {
            font:{
                size:14 ,
                family: "IRANSansWeb",
            },
        },
      },
      };
      
      const labels = ["فروش محصولات"];

      const setChart = dataApi.map(item => (
        {
          label : item.title,
          data : labels.map(() => item.count),
          backgroundColor : `rgb(${Math.floor(Math.random()*255)},${Math.floor(Math.random()*255)},${Math.floor(Math.random()*255)} )`
        }
      ));
    
      const data = {
        labels,
        datasets:setChart,
      };

              

    return (
        <div className={style.mainChart}>
            <Bar options={options} data={data} />
        </div>
    );
};

export default VerticalChart;