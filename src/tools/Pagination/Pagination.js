import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import { Link } from "react-router-dom";
import Row from "../tableRow/Row";
import "./Pagination.css";
import EditIcon from "@material-ui/icons/Edit";
const Paginate = (props) => {
  const [pageNumber, setPageNumber] = useState(0);
  // useEffect(()=>{
  // setPreload(false)
  // },[])


  const perPage = props.perpage;
  const pageVisited = pageNumber * perPage;

  const displayCourse = props.datas
    .slice(pageVisited, pageVisited + perPage)
    .map((item) => {
      return (
        <>
          <Row
            data={Object.values(item)}
            link={props.link + item.id}
          />
        </>
      );
    });
  
  const chngePage = ({ selected }) => {
    setPageNumber(selected);
    /* const test = document.getElementsByClassName(style.transition);
    for (let i = 0; i < test.length; i++) {
      test[i].classList.remove(style.transition);
    } */
  };
  const pageCount = Math.ceil(props.datas.length / perPage);
  return (
    <>
      <div>{displayCourse.length === 0 ? null : displayCourse}</div>
      <ReactPaginate
        previousLabel="صفحه قبلی"
        nextLabel="صفحه بعد"
        pageCount={pageCount}
        onPageChange={chngePage}
        pageRangeDisplayed={2}
        marginPagesDisplayed={1}
        renderOnZeroPageCount={null}
        containerClassName={"pagination"}
        previousLinkClassName={"prev-btn"}
        nextLinkClassName={"next-btn"}
        disabledClassName={"disable-page"}
        activeClassName={"active-page"}
      />
    </>
  );
};

export default Paginate;
